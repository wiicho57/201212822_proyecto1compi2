package a201212822_proyecto1compi2;

import java_cup.runtime.Symbol;
import java_cup.runtime.*;
import java.lang.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.util.ArrayList;

%%
%class Lexico
%cup
%full
%public
%line
%char
%ignorecase
%eofval{
return new Symbol(sym.EOF,new String("Fin de Archivo"));
%eofval}

letra=[A-Za-z]
numero=[0-9]
bool = "true" | "false"
asocia = "Izquierda" | "Derecha"

%%


"Dec"					{System.out.println("Dec");
						return new Symbol(sym.T_dec , yychar, yyline, new String(yytext()));}

"<"						{System.out.println("menor que");
						return new Symbol(sym.T_menor , yychar, yyline, new String(yytext()));}

">"						{System.out.println("mayor que");
						return new Symbol(sym.T_mayor , yychar, yyline, new String(yytext()));}
						
"/"						{System.out.println("divicion");
						return new Symbol(sym.T_divicion , yychar, yyline, new String(yytext()));}

"Gram"					{System.out.println("gram");
						return new Symbol(sym.T_gram , yychar, yyline, new String(yytext()));}

"Cod"					{System.out.println("cod");
						return new Symbol(sym.T_cod , yychar, yyline, new String(yytext()));}

"nonTerminal"			{System.out.println("nonTerminal");
						return new Symbol(sym.T_nonterminal , yychar, yyline, new String(yytext()));}

"tipo"					{System.out.println("tipo");
						return new Symbol(sym.T_tipo , yychar, yyline, new String(yytext()));}						

"Lista"					{System.out.println("lista");
						return new Symbol(sym.T_lista , yychar, yyline, new String(yytext()));}

"nombre"				{System.out.println("nombre");
						return new Symbol(sym.T_nombre , yychar, yyline, new String(yytext()));}							
							
"precedencia"			{System.out.println("precedencia");
						return new Symbol(sym.T_precedencia , yychar, yyline, new String(yytext()));}	

"terminal"				{System.out.println("terminal");
						return new Symbol(sym.T_terminal , yychar, yyline, new String(yytext()));}

"asociatividad"			{System.out.println("asociatividad");
						return new Symbol(sym.T_asociatividad , yychar, yyline, new String(yytext()));}

"asociacion"			{System.out.println("asociacion");
						return new Symbol(sym.T_asociacion, yychar, yyline, new String(yytext()));}

"inicio"				{System.out.println("inicio");
						return new Symbol(sym.T_inicio, yychar, yyline, new String(yytext()));}

"sim"					{System.out.println("sim");
						return new Symbol(sym.T_sim, yychar, yyline, new String(yytext()));}
						
":"						{System.out.println(":");
						return new Symbol(sym.T_dospuntos, yychar, yyline, new String(yytext()));}						

"="						{System.out.println("=");
						return new Symbol(sym.T_igual, yychar, yyline, new String(yytext()));}	

"|"						{System.out.println("|");
						return new Symbol(sym.T_pepilin, yychar, yyline, new String(yytext()));}							
						
";"						{System.out.println(";");
						return new Symbol(sym.T_puntoycoma, yychar, yyline, new String(yytext()));}		

"$"						{System.out.println("$");
						return new Symbol(sym.T_dolar, yychar, yyline, new String(yytext()));}
						
"."						{System.out.println(".");
						return new Symbol(sym.T_punto, yychar, yyline, new String(yytext()));}
						
"{"						{System.out.println("{");
						return new Symbol(sym.T_abrirllave, yychar, yyline, new String(yytext()));}
						
"}"						{System.out.println("}");
						return new Symbol(sym.T_cerrarllave, yychar, yyline, new String(yytext()));}												

"<:"				    {System.out.println("<:");
						return new Symbol(sym.T_accionabrir, yychar, yyline, new String(yytext()));}

":>"					{System.out.println(":>");
						return new Symbol(sym.T_accioncerrar, yychar, yyline, new String(yytext()));}						

"upg"					{System.out.println("upg");
						return new Symbol(sym.T_upg, yychar, yyline, new String(yytext()));}
						

","						{System.out.println(",");
						return new Symbol(sym.T_coma, yychar, yyline, new String(yytext()));}						
						
"import"				{System.out.println("import");
						return new Symbol(sym.T_import, yychar, yyline, new String(yytext()));}	

"int"						{System.out.println("int");
							return new Symbol(sym.T_tint, yychar, yyline, new String(yytext()));}

"char"						{System.out.println("char");
							return new Symbol(sym.T_tchar, yychar, yyline, new String(yytext()));}

"double"				    {System.out.println("double");
							return new Symbol(sym.T_tdouble, yychar, yyline, new String(yytext()));}

"string"					{System.out.println("string");
							return new Symbol(sym.T_tstring, yychar, yyline, new String(yytext()));}

"bool"						{System.out.println("bool");
							return new Symbol(sym.T_tbool, yychar, yyline, new String(yytext()));}

"["						    {System.out.println("[");
							return new Symbol(sym.T_abrircorchete, yychar, yyline, new String(yytext()));}
							
"]"							{System.out.println("]");
							return new Symbol(sym.T_cerrarcorchete, yychar, yyline, new String(yytext()));}

"public"					{System.out.println("public");
							return new Symbol(sym.T_public, yychar, yyline, new String(yytext()));}

"private"					{System.out.println("private");
							return new Symbol(sym.T_private, yychar, yyline, new String(yytext()));}	

"protected"					{System.out.println("protected");
							return new Symbol(sym.T_protected, yychar, yyline, new String(yytext()));}

"class"						{System.out.println("class");
							return new Symbol(sym.T_class, yychar, yyline, new String(yytext()));}	

"("						    {System.out.println("(");
							return new Symbol(sym.T_abrirparentesis, yychar, yyline, new String(yytext()));}
							
")"						    {System.out.println(")");
							return new Symbol(sym.T_cerrarparentesis, yychar, yyline, new String(yytext()));}

"new"						{System.out.println("new");
							return new Symbol(sym.T_new, yychar, yyline, new String(yytext()));}

"void"						{System.out.println("void");
							return new Symbol(sym.T_void, yychar, yyline, new String(yytext()));}

"if"						{System.out.println("if");
							return new Symbol(sym.T_if, yychar, yyline, new String(yytext()));}
							
"else"					    {System.out.println("else");
							return new Symbol(sym.T_else, yychar, yyline, new String(yytext()));}
							
"elseif"				    {System.out.println("elseif");
							return new Symbol(sym.T_elseif, yychar, yyline, new String(yytext()));}
							
"switch"				    {System.out.println("switch");
							return new Symbol(sym.T_switch, yychar, yyline, new String(yytext()));}
							
"case"						{System.out.println("case");
							return new Symbol(sym.T_case, yychar, yyline, new String(yytext()));}
							
"default"				    {System.out.println("default");
							return new Symbol(sym.T_default, yychar, yyline, new String(yytext()));}

"while"						{System.out.println("while");
							return new Symbol(sym.T_while, yychar, yyline, new String(yytext()));}
							
"do"						{System.out.println("Do");
							return new Symbol(sym.T_do, yychar, yyline, new String(yytext()));}
							
"repeat"				    {System.out.println("repeat");
							return new Symbol(sym.T_repeat, yychar, yyline, new String(yytext()));}

"until"					    {System.out.println("until");
							return new Symbol(sym.T_until, yychar, yyline, new String(yytext()));}

"for"					    {System.out.println("for");
							return new Symbol(sym.T_for, yychar, yyline, new String(yytext()));}

"loop"						{System.out.println("loop");
							return new Symbol(sym.T_loop, yychar, yyline, new String(yytext()));}

"print"					    {System.out.println("print");
							return new Symbol(sym.T_print, yychar, yyline, new String(yytext()));}

"ParseInt"				    {System.out.println("ParseInt");
							return new Symbol(sym.T_parseint, yychar, yyline, new String(yytext()));}

"ParseDouble"				{System.out.println("ParseDouble");
							return new Symbol(sym.T_parsedouble, yychar, yyline, new String(yytext()));}
							
"intToSTR"				    {System.out.println("intToSTR");
							return new Symbol(sym.T_inttostr, yychar, yyline, new String(yytext()));}
							
"doubleToSTR"			    {System.out.println("doubleToSTR");
							return new Symbol(sym.T_doubletostr, yychar, yyline, new String(yytext()));}
							
"doubleToInt"			    {System.out.println("doubleToInt");
							return new Symbol(sym.T_doubletoint, yychar, yyline, new String(yytext()));}
							
"return"				    {System.out.println("return");
							return new Symbol(sym.T_return, yychar, yyline, new String(yytext()));}
							
"||"				        {System.out.println("or");
							return new Symbol(sym.T_or, yychar, yyline, new String(yytext()));}

"&&"				        {System.out.println("and");
							return new Symbol(sym.T_and, yychar, yyline, new String(yytext()));}

"?"					        {System.out.println("negado");
							return new Symbol(sym.T_negado, yychar, yyline, new String(yytext()));}							
							
"??"					    {System.out.println("xor");
							return new Symbol(sym.T_xor, yychar, yyline, new String(yytext()));}							
							
"+"				            {System.out.println("+");
							return new Symbol(sym.T_mas, yychar, yyline, new String(yytext()));}

"-"				           {System.out.println("for");
							return new Symbol(sym.T_menos, yychar, yyline, new String(yytext()));}
							
"*"				           {System.out.println("*");
							return new Symbol(sym.T_por, yychar, yyline, new String(yytext()));}
							
					
"^"				           {System.out.println("^");
							return new Symbol(sym.T_exponencial, yychar, yyline, new String(yytext()));}
							
"++"				        {System.out.println("++");
							return new Symbol(sym.T_masmas, yychar, yyline, new String(yytext()));}
							
"--"				        {System.out.println("--");
							return new Symbol(sym.T_menosmenos, yychar, yyline, new String(yytext()));}
							
							
">="			            {System.out.println(">=");
							return new Symbol(sym.T_mayorigual, yychar, yyline, new String(yytext()));}
							
"<="				        {System.out.println("<=");
							return new Symbol(sym.T_menorigual, yychar, yyline, new String(yytext()));}
							
"=="				        {System.out.println("==");
							return new Symbol(sym.T_igualigual, yychar, yyline, new String(yytext()));}
							
"!="				        {System.out.println("!=");
							return new Symbol(sym.T_distintode, yychar, yyline, new String(yytext()));}

"extends"				    {System.out.println("extends");
							return new Symbol(sym.T_extends, yychar, yyline, new String(yytext()));}							

"break"				    	{System.out.println("break");
							return new Symbol(sym.T_break, yychar, yyline, new String(yytext()));}

"super"				    	{System.out.println("super");
							return new Symbol(sym.T_super, yychar, yyline, new String(yytext()));}	

"THIS"				    	{System.out.println("this");
							return new Symbol(sym.T_this, yychar, yyline, new String(yytext()));}							

							
{bool}						{System.out.println("bool: "+yytext());
							return new Symbol(sym.T_bool, yychar, yyline, new String(yytext()));}	
						
{asocia}					{System.out.println("asociacion : "+ yytext());
							return new Symbol(sym.T_asocia , yychar, yyline, new String(yytext()));}						

("/"{letra}({letra}|{numero}|_)*)(("/"{letra}({letra}|{numero}|_)*)*)".upg"
							{System.out.println("url : "+ yytext());
							return new Symbol(sym.T_url , yychar, yyline, new String(yytext()));}
							
{letra}({letra}|{numero}|_)* {System.out.println("id: "+yytext());
							return new Symbol(sym.T_id, yychar, yyline, new String(yytext()));}
							
{numero}+			 		{System.out.println("numero: "+yytext());
							return new Symbol(sym.T_numero, yychar, yyline, new String(yytext()));}

{numero}+("."{numero}+) 	{System.out.println("numero: "+yytext());
							return new Symbol(sym.T_ndouble, yychar, yyline, new String(yytext()));}
							
\"[^(\n\t\r\f)]*\" 			{System.out.println("cadena: "+yytext());
							return new Symbol(sym.T_cadena, yychar, yyline, new String(yytext()));}
							
\'[^"\'"]\' 				{System.out.println("char: "+yytext());
							return new Symbol(sym.T_char, yychar, yyline, new String(yytext()));}
							
"/*"[^"*/"]*"*/"			{System.out.println("comentario bloque:\n"+yytext());}
"//"[^\n]*\n				{System.out.println("comentario linea:\n"+yytext());}

[ \t\r\f] {}
[\n]  { yychar=-1;}
 
.   {
        JOptionPane.showMessageDialog(null, "Error Lexico vino : "+yytext()+" en la linea "+(yyline+1)+" y columna "+(yychar+1));
        //ErrorEncontrado errorEncontrado=new ErrorEncontrado("Lexico",yyline+1,yychar+1,yytext());
        //lista.add(errorEncontrado);
        //huboError=true;
		// ~
    }						
							
						
						
						
						
						
						
						
						
						
						
						
						
						
						
						
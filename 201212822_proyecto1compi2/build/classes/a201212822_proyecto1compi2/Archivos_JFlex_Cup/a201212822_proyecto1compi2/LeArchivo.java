/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a201212822_proyecto1compi2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luis
 */
public class LeArchivo {
     //almacena el contenido del archivo abierto
    public String texto="";
    
    /**
     * 
     * @return regresa el contenido del archivo
     */
    public String getTexto(){
        return texto;
    }
    
    /**
     * 
     * @param nombre_archivo recibe el nombre o ruta del archivo
     * @param texto_escribir recibe el contenido modificado del texto
     */
     public void escribir(String nombre_archivo, String texto_escribir){
       
        
        File f;                                         //se crea una variable tipo File
        f = new File(nombre_archivo);                   //se inicializa la variable con el valor del metodo 
       
        try{
            FileWriter w = new FileWriter(f);           //se inicializa la una variable de escritura de archivo
            BufferedWriter bw = new BufferedWriter(w);  
            PrintWriter wr = new PrintWriter(bw); 
            
            //Con nuestro PrintWriter ingresamos el texto que deseamos escribir
            wr.write(texto_escribir);
            wr.close();
            bw.close();
        }catch(IOException e){
            System.out.println("Error al escribir");
        }

 }
     
    //Para la lectura de archivos se usan los mismos elementos con la diferencia que estos tienen el sufijo Reader 
    public void Leer(String nombre_archivo){
        
        try {
            File archivo;
            FileReader fr;
            BufferedReader br;
            archivo=new File(nombre_archivo);
            fr = new FileReader (archivo);
            br=new BufferedReader(fr);
            
            String linea;
            try {
                //Lee cada linea y la concatena a un string global
                while((linea=br.readLine())!=null){
                     texto=texto+linea+"\n";
                }
            } catch (IOException ex) {
                Logger.getLogger(LeArchivo.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LeArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
     
     
     
}


package a201212822_proyecto1compi2;

import java.util.ArrayList;

/**
 *
 * @author luis
 */
public class nodo_arbol {
    
    //lexema 
    String lexema;
    //parametro token 
    String token;
    //parametro fila
    int fila;
    //parametro columna 
    int columna;
    //lista para guardar los hijos
    ArrayList<nodo_arbol> hijos;
    
    //contructor de la clase 
    public nodo_arbol (String lexema, String token, int fila, int columna)
    {
        this.lexema = lexema;
        this.token = token;
        this.fila = fila;
        this.columna = columna;
        this.hijos = new ArrayList<nodo_arbol>();
    }
    
    /**
     * 
     * @param hijo 
     * metodo para agregar hijos al nodo padre
     */
    public void AgregarHijo(nodo_arbol hijo){
        this.hijos.add(hijo);
    }
        
    /**
     * 
     * @param lexema lexema del nodo
     */
    public void ingresarLexema(String lexema)
    {
        this.lexema = lexema;
    }
    
    /**
     * 
     * @param token ingreso del token al nodo
     */
    public void ingresarToken(String token)
    {
        this.token = token;
    }
    
    /**
     * 
     * @param fila ingreso de la fila del token
     */
    public void ingresarFila(int fila)
    {
        this.fila = fila;
    }
    
    /**
     * 
     * @param columna ingreso de la columna del token
     */    
    public void ingresarColumna(int columna)
    {
        this.columna = columna;
    }
    
    /**
     * 
     * @return retorno del lexema del nodo
     */
    public String obtenerLexema()
    {
        return this.lexema;
    }
    
    /**
     * 
     * @return regresa el valor del token que posee
     */
    public String obtenerToken(){
        return this.token;
    }
    
    /**
     * 
     * @return la fila en la que se encuentra el toke
     */
    public int obtenerFila(){
        return this.fila;
    }
    
    /**
     * 
     * @return la columna en la que se encuentra el token
     */
    public int obtenerColumna(){
        return this.columna;
    }   
}

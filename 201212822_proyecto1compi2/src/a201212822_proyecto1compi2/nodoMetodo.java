/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a201212822_proyecto1compi2;

/**
 *
 * @author luis
 */
public class nodoMetodo {
    
    private String nombre;
    private String retorno;
    private int numero_parametros; 
    private nodo_arbol raiz;
    
    public nodoMetodo(String nombre,String retorno,int num_para,nodo_arbol raiz)
    {
        this.nombre = nombre;
        this.retorno = retorno;
        this.numero_parametros = num_para;
        this.raiz = raiz;
        
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the retorno
     */
    public String getRetorno() {
        return retorno;
    }

    /**
     * @param retorno the retorno to set
     */
    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    /**
     * @return the numero_parametros
     */
    public int getNumero_parametros() {
        return numero_parametros;
    }

    /**
     * @param numero_parametros the numero_parametros to set
     */
    public void setNumero_parametros(int numero_parametros) {
        this.numero_parametros = numero_parametros;
    }

    /**
     * @return the raiz
     */
    public nodo_arbol getRaiz() {
        return raiz;
    }

    /**
     * @param raiz the raiz to set
     */
    public void setRaiz(nodo_arbol raiz) {
        this.raiz = raiz;
    }
    
}
